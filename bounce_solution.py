
# Based on 
#
# Animation of Elastic collisions with Gravity
# 
# author: Jake Vanderplas
# email: vanderplas@astro.washington.edu
# website: http://jakevdp.github.com
# license: BSD
# Please feel free to use and modify this, but keep the above information. Thanks!


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

dots = np.random.rand(100,4)
dots[:,2:] -= 0.5
dots[:,2:] *= 0.01

def main():
    fig, ax = plt.subplots()

    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    
    xs = dots[:,0]
    ys = dots[:,1]
    
    line, = ax.plot([],[],'b.',ms=16)
    
    def animate(i):
        timestep()
        line.set_data(xs,ys)  # update the data
        return line,
    
    ani = animation.FuncAnimation(fig, animate, 
                                  interval=25, 
                                  blit=True)

    # ani.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
    # or
    plt.show()

def timestep():
    dots[:,:2] += dots[:,2:]

    # gravity
    #dots[:,3]  -= 0.001

    collision()

    cross_left  = dots[:,0] < 0.
    cross_right = dots[:,0] > 1.
    cross_top   = dots[:,1] > 1.
    cross_bot   = dots[:,1] < 0.

    dots[cross_left | cross_right, 2] *= -1.
    dots[cross_top  | cross_bot,   3] *= -1.

    # fix out-of-boundary items
    dots[cross_left, 0] = 0.
    dots[cross_right,0] = 1.

    dots[cross_top, 1] = 1.
    dots[cross_bot, 1] = 0.


def collision():
    from scipy.spatial.distance import pdist, squareform
    D = squareform(pdist(dots[:, :2]))
    ind1, ind2 = np.where(D < 0.04)

    # pick only top diagonal, no repetition
    unique = (ind1 < ind2)
    ind1 = ind1[unique]
    ind2 = ind2[unique]

    # now we only have a few interesting dots
    # explicit python loop is not so expensive
    for i1, i2 in zip(ind1, ind2):
        # location vector
        r1 = dots[i1, :2]
        r2 = dots[i2, :2]
        # velocity vector
        v1 = dots[i1, 2:]
        v2 = dots[i2, 2:]
        # relative location & velocity vectors
        r_rel = r1 - r2
        v_rel = v1 - v2
        # vector of the center of mass
        v_cm = (v1 + v2) / 2.
        # collisions of spheres reflect v_rel over r_rel
        rr_rel = np.dot(r_rel, r_rel)
        vr_rel = np.dot(v_rel, r_rel)
        v_rel = v_rel - 2 * r_rel * vr_rel / rr_rel
        # assign new velocities
        dots[i1, 2:] = v_cm + 0.5 * v_rel
        dots[i2, 2:] = v_cm - 0.5 * v_rel
        # crude avoidance of glue effect
        dots[i1, :2] += 0.1*r_rel
        dots[i2, :2] -= 0.1*r_rel

main()

