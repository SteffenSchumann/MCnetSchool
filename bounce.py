import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# 100 particles, with x,y,vx,vy
dots = np.random.rand(100,4)
# velocities on -0.5,0.5
dots[:,2:] -= 0.5
# scale velocities
dots[:,2:] *= 0.01

def main():
    fig, ax = plt.subplots()

    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    
    xs = dots[:,0]
    ys = dots[:,1]
    
    line, = ax.plot([], [], 'b*', ms=16)

    def animate(i):
        timestep()
        line.set_data(xs,ys)  # update the data
        return line,
    
    ani = animation.FuncAnimation(fig, animate, 
                                  interval=25, 
                                  blit=True)

    # ani.save('basic_animation.mp4', fps=30, 
    #          extra_args=['-vcodec', 'libx264'])
    #or
    plt.show()


def timestep():
    dots[:,:2] += dots[:,2:]

    touching_floor = dots[:,1] < 0.0

    dots[ touching_floor , 3  ] *= -1.0

main()













